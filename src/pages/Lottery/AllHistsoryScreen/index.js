import {
  Button, Empty, Input, Modal, Table,
} from 'antd';
import React, { useEffect, useMemo, useState } from 'react';
import BaseAPI from '../../../utils/BaseAPI';

import './style.scss';

const AllHistoryScreen = () => {
  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [keyword, setKeyword] = useState(null);
  const fetchUsers = async () => {
    setIsLoading(true);
    try {
      const response = await BaseAPI.get('/lottery/all?limit=100000');

      if (response.status) {
        setList(response.data.docs);
      }
    } catch (e) {
      //
    }
    setIsLoading(false);
  };

  const computedList = useMemo(() => {
    if (!keyword) {
      return list;
    }

    return list.filter((it) => (
      it.phoneNumber.indexOf(keyword) >= 0 || it.name.indexOf(keyword) >= 0
    ));
  }, [keyword, list]);

  const onChangeKeyword = (e) => {
    const { value } = e.target;
    setKeyword(value);
  };

  useEffect(() => {
    fetchUsers();
  }, []);

  const columns = [
    {
      title: 'Số điện thoại',
      dataIndex: 'phoneNumber',
      key: 'phoneNumber',
    },
    {
      title: 'Tên',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Số dư',
      dataIndex: 'balance',
      key: 'balance',
      render: (cell) => new Intl.NumberFormat('vi-VN', {
        style: 'currency',
        currency: 'VND',
      }).format(cell),
    },
    {
      title: 'Mở khoá',
      dataIndex: 'id',
      align: 'right',
      render: () => (
        <>
          <Button type="danger" onClick={() => Modal.error({ title: 'Đang hoàn thiện', content: 'Chức năng đang hoàn thiện' })}>
            Khoá tài khoản
          </Button>
        </>
      ),
    },
  ];

  return (
    <div>
      <div className="keyword-row">
        <h1>Danh sách tài khoản</h1>
        <div className="search">
          <Input.Search
            placeholder="Nhập SĐT Hoặc tên"
            onChange={onChangeKeyword}
          />
        </div>
      </div>
      <Table
        loading={isLoading}
        dataSource={[...computedList]}
        pagination={{ pageSize: 10 }}
        columns={columns}
        locale={{
          emptyText: <Empty description="Chưa có dữ liệu hoặc không tìm thấy" />,
        }}
      />
    </div>
  );
};

export default AllHistoryScreen;
