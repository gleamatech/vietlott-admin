import React from 'react'
import './style.scss'

const SampleButton = () => {
    return(
        <div className="sample-button">
            Your sample here
        </div>
    )
}

export default React.memo(SampleButton)