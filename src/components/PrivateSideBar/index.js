import React from 'react';
import { useSelector } from 'react-redux';
import { Route } from 'react-router';
import { Redirect } from 'react-router-dom';

const PrivateSideBar = ({ isPrivate, roles, ...props }) => {
  const user = useSelector((state) => state.user.user);

  if (roles && !roles.includes(user.role) && user.role !== 'admin') {
    return <Redirect to='/' />;
  }

  return <Route {...props} />;
};

export default PrivateSideBar;
