const permissions = [
    { label: 'Nạp Tiền', value: 'deposit' },
    { label: 'Rút tiền', value: 'withdraw' },
    { label: 'Danh sách tài khoản', value: 'userList' },
    { label: 'Khoá tài khoản', value: 'banUser' },
    { label: 'Thống kê bán hàng', value: 'saleBoard' },
    { label: 'Lịch sử giao dịch', value: 'transactionList' },
    { label: 'Lịch sử rút tiền', value: 'withdrawList' },
    { label: 'Lịch sử vé bán', value: 'saleList' },
    { label: 'Lịch sử vé bán (Web)', value: 'saleListWeb' },
    { label: 'Vé truyền thống', value: 'ticket' },
  ]