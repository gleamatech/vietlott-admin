export const formatCurrency = (money) => {
  const num = parseInt(money)
  return `${(num || 0).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}đ`
}
